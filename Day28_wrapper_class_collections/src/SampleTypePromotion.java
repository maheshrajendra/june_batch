
public class SampleTypePromotion {
	static void print(byte b) {
		System.out.println("byte method "+ b);
	}

	static void print(int i) {
		System.out.println("int method " + i);
	}

	static void print(double d) {
		System.out.println("double method " + d);
	}

	static void print(Short s) {
		System.out.println("Short non primitive " + s);
	}
	
	static void print(Byte b) {
		System.out.println("Byte non primitive "+b);
	}

	public static void main(String[] args) {
		char c = 'A';
		print(c);
	}
}
