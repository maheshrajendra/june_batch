import java.util.ArrayList;

public class SampleList {
	public static void main(String[] args) {
		ArrayList list = new ArrayList();
		list.add(45);
		list.add(50);
		list.add("hello");
		list.add(4.5);
		list.add('b');
		list.add(true);
		list.add(null);
		System.out.println(list); // [45, 50, hello, 4.5, b, true, null]
		System.out.println(list.size()); // 7
		System.out.println(list.isEmpty()); // false
		System.out.println(list.contains(10)); // false
		System.out.println(list.contains(4.5)); // true
		System.out.println(list.remove(new Integer(45)));
		System.out.println(list); // [50, hello, 4.5, b, true, null]
		list.clear();
		System.out.println(list);
		
	}
}
