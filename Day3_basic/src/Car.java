
public class Car {
	String brandName = "BMW";
	double price = 7500000;
	String model = "GLS";
	int fuelCapacity = 45;
	String color = "blue";
	boolean isAutomatic = true;

	public static void main(String[] args) {
		Car c1 = new Car();
		System.out.println(c1.color); // blue
		c1.color = "red";
		System.out.println(c1.color); // red
		System.out.println(c1.brandName); // BMW
		System.out.println(c1.fuelCapacity); // 45

		Car c2 = new Car();
		c2.color = "Black";
		System.out.println(c2.color);
	}
}
