
public class Duck {
	char gender; // \u0000
	String color = "yellow";
	int noOfLegs = 2;

	public static void main(String[] args) {
		Duck d1 = new Duck();
		d1.gender = 'M';
		System.out.println(d1.color); // yellow
		System.out.println(d1.gender); // M
		System.out.println(d1.noOfLegs); // 2

		Duck d2 = new Duck();
		d2.gender = 'F';
		System.out.println(d2.gender); // F
	}
}
