import java.util.Arrays;

public class Student {
	int id;
	String name;
	int age;

	public Student(int id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + "]";
	}

	public static void main(String[] args) {
		Student s1 = new Student(1, "Alpha", 23);
		Student s2 = new Student(2, "Beta", 24);
		Student s3 = new Student(3, "Charlie", 20);

		// how to create non primitive Student type array and storing the elements in
		// it.
		Student[] studentsData = { s1, s2, s3 };

		// using a for each loop iterating a non-primitive Student type array
		for (Student s : studentsData) {
			System.out.println(s);
		}
		System.out.println("__________________________");

		// using for loop to print studentsData array
		for (int i = 0; i < studentsData.length; i++) {
			System.out.println(studentsData[i]);
		}
		
		System.out.println("_____________________");

		// using Arrays.toString() to print array
		System.out.println(Arrays.toString(studentsData));
	}

}
