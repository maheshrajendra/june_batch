import java.util.Arrays;

public class ArraySamples {
	static void fillArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			arr[i] = i + 1;
		}
	}

	public static void main(String[] args) {
		int[] nums = new int[3];

		fillArray(nums);

		System.out.println(Arrays.toString(nums));
	}
}
