package monkeyexample;

public class Monkey {
	void eat(Banana b) {
		System.out.println("Monkey eats banana");
	}

	void eat(Chocolate c) {
		System.out.println("Monkey eats Chocolate");
	}

	public static void main(String[] args) {
		Monkey m = new Monkey();
		Banana b = new Banana();
		m.eat(b);

		Chocolate chocki = new Chocolate();
		m.eat(chocki);

	}
}
