package amazonexample;

public class Amazon {
	void pay(String upiAddress) {
		System.out.println("Payment succesful using upiAddress " + upiAddress);
	}

	void pay(DebitCard d) {
		System.out.println("payment successful using debit card " + d.cardNo);
	}

	void pay(CreditCard c) {
		System.out.println("payment successful using credit card " + c.cardNo);
	}

	public static void main(String[] args) {
		Amazon a = new Amazon();
		a.pay("87654567@ybl");

		DebitCard card = new DebitCard();
		card.name = "Alpha";
		card.cvv = 345;
		card.cardNo = 987654326789L;

		a.pay(card);

		CreditCard cc = new CreditCard();
		cc.name = "Beta";
		cc.cvv = 874;
		cc.cardNo = 987635465362765L;

		a.pay(cc);
	}
}
