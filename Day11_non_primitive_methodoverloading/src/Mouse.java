
public class Mouse {
	String brand;
	double price;
	String color;
	String type;

	// override the toString() method -> ALT + SHIFT + S + S
	@Override
	public String toString() {
		return "Mouse [brand=" + brand + ", price=" + price + ", color=" + color + ", type=" + type + "]";
	}

	public static void main(String[] args) {
		Mouse m1 = new Mouse();
		m1.color = "Black";
		// when we print the reference of the object
		// it given address of the object
		System.out.println(m1);

		Mouse m2 = new Mouse();
		m2.color = "White";
		System.out.println(m2);
	}

}
