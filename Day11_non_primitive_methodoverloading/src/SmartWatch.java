
public class SmartWatch {

	String brand;
	String color;
	double price;

	public static void main(String[] args) {
		SmartWatch s = new SmartWatch();
		s.brand = "Fossil";
		s.price = 10999;
		s.color = "Silver";
		System.out.println(s);
	}

	// overriding the toString() -> alt + shift + s + s
	@Override
	public String toString() {
		return "SmartWatch [brand=" + brand + ", color=" + color + ", price=" + price + "]";
	}

}
