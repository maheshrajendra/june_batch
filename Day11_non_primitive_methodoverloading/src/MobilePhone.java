
public class MobilePhone {
	void unlock(int pin) {
		System.out.println("Unlocked phone using pin number");
	}

	void unlock(String password) {
		System.out.println("Unlocked phone using password");
	}

	void unlock(FingerPrint f) {
		System.out.println("Unlocked phone using FingerPrint");
	}

	public static void main(String[] args) {
		MobilePhone m = new MobilePhone();
		m.unlock(1234);
		m.unlock("Alpha@123");
		m.unlock(new FingerPrint());
	}
}

class FingerPrint {

}
