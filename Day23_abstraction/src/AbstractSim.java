
public abstract class AbstractSim {
	void sos() {
		System.out.println("Emergency calling...");
	}

	abstract void call();

	abstract void sms();

}
