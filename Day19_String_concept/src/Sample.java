
public class Sample {
	public static void main(String[] args) {
		String x = "Hello";
		String a = new String("Hello");
		System.out.println(x == a); // false
		System.out.println(x.equals(a)); // true
		String z = "hello";
		System.out.println(x.equals(z)); // false
		System.out.println(x.equalsIgnoreCase(z)); // true
	}
}
