
public class StringReverse {
	public static void main(String[] args) {
		String s = "good Morning";
		for (int i = s.length() - 1; i >= 0; i--) {
			System.out.print(s.charAt(i));
		}

		System.out.println();
		System.out.println("________________");

		// creating a StringBuilder object using a given String
		// then calling the reverse() of the StringBuilder object
		// then converting the StringBuilder object to the String format
		System.out.println(new StringBuilder(s).reverse().toString());
	}
}
