
public class SampleBuilder {
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		System.out.println(sb.capacity()); // 16
		sb.append("hello");
		System.out.println(sb.length()); // 5
		System.out.println(sb.capacity()); // 16
		sb.append("abcdefghijk");
		System.out.println(sb.capacity()); // 16
		System.out.println(sb.length()); // 16
		sb.append("l");
		System.out.println(sb.capacity()); // 34 (increase with
		// nc = cc * 2 + 2;
		System.out.println(sb.length()); // 17
		sb.append("wertyuqiwieirieid");
		System.out.println(sb.capacity()); // 34
		System.out.println(sb.length()); // 34
		sb.append("world");
		System.out.println(sb.capacity());
		System.out.println(sb.length());
		sb.append("sdfjshfkshfkshdfkshdfkhsfhsfkhskfhskdfhskdhfshf");
		System.out.println(sb.capacity());
		System.out.println(sb.length());
	}
}
