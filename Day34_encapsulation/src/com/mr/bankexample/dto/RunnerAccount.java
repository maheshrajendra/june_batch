package com.mr.bankexample.dto;

public class RunnerAccount {
	public static void main(String[] args) {
		Account a = new Account();
		a.setAccountNo(34234234L);
		a.setAccountHolderName("Alpha");
		a.setBalance(1500);

		System.out.println(a);
	}
}
