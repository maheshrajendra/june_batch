package com.mr.bankexample.dto;

public class Account {
	// all data members(variables) must be declared private
	private long accountNo;
	private String accountHolderName;
	private double balance;

	// each data member must have corresponding public data handler methods
	// (i.e., getter and setter methods) -> ALT + SHIFT + S, R
	public long getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(long accountNo) {
		this.accountNo = accountNo;
	}

	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [accountNo=" + accountNo + ", accountHolderName=" + accountHolderName + ", balance=" + balance
				+ "]";
	}

}
