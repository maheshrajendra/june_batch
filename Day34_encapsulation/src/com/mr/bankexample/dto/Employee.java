package com.mr.bankexample.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Employee {

	private String name;
	private int id;
	private double salary;

}
