
public class Employee {
	int empId;
	String name;
	static String companyName = "GOOGLE";

	public Employee(int empId, String name) {
		this.empId = empId;
		this.name = name;
	}

	public static void main(String[] args) {
		System.out.println(Employee.companyName); // GOOGLE
		Employee e1 = new Employee(1, "Alpha");
		System.out.println(e1.empId);
		System.out.println(e1.name);
		System.out.println(e1.companyName); 	
		Employee.companyName = "Alphabet INC.";
		System.out.println(e1.companyName);
		Employee e2 = new Employee(2, "Beta");
		System.out.println(e2.empId);
		System.out.println(e2.name);
		System.out.println(e2.companyName);
		System.out.println(e1.companyName);
		System.out.println(Employee.companyName);
	}
}
