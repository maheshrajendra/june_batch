
public class SimpleInterest {
	static double findSimpleInterest(double p, int t, double r) {
		return (p * t * r) / 100;
	}

	public static void main(String[] args) {
		double result = findSimpleInterest(100000, 1, 5.5);
		System.out.println("Base rate is " + result);
		System.out.println("Base rate with tax is " + (result + 100));
	}
}
