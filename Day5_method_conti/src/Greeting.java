
public class Greeting {
	static String concat(String s1, String s2) {
		return s1 + " " + s2;
	}

	static String greet(String name) {
		return "Hello " + name;
	}

	public static void main(String[] args) {
		System.out.println(concat("Good", "Morning"));

		String message = concat("Good", "Afternoon");
		System.out.println(message); // Good Afternoon
		System.out.println(message); // Good Afternoon
		System.out.println(message); // Good Afternoon

		// call the greet method with your name and store it in greeting variable

		System.out.println(greet("Mahesh MR")); // Hello Mahesh MR

		System.out.println(greet("Charlie")); // Hello Charlie

	}
}
