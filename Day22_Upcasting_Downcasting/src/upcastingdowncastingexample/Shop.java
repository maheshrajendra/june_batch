package upcastingdowncastingexample;

public class Shop {
	Fruit sellFruit(String name) {
		if (name != null) {
			if (name.equalsIgnoreCase("Apple")) {
				Apple a = new Apple();
				a.color = "Red";
				a.price = 200;
				return a;
			} else if (name.equalsIgnoreCase("PineApple")) {
				PineApple p = new PineApple();
				p.color = "Green";
				p.price = 80;
				return p;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		Shop s = new Shop();
		Fruit f = s.sellFruit("Apple"); // Fruit f = new Apple();
		if (f != null) {
			System.out.println(f.color); // Red
			System.out.println(f.price); // 200
			f.clean(); // Cleaning the fruit...
			// f.prepareMilkShake(); will not work as it is a sub class specific member
			// even though the object is of Apple type, when we upcast the sub class
			// specific member is not accessible
			if (f instanceof Apple) {
				Apple a = (Apple) f;
				a.prepareMilkShake(); // Preparing apple milk shake
			} else if (f instanceof PineApple) {
				PineApple p = (PineApple) f;
				p.prepareSalad(); // Preparing pineapple salad
			}
		} else {
			System.out.println("No such fruit...");
		}
	}

}
