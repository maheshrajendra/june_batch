package upcastingdowncastingexample;

public class Printer {
	void print(String name) {
		if (name != null) // avoiding a null pointer exception by checking it.
		{
			System.out.println(name.toUpperCase());
		}
	}

	public static void main(String[] args) {
		Printer p = new Printer();
		// p.print(null);
		p.print("Alpha");
	}
}
