
public class Calculator {
	static void square(int n) {
		System.out.println(n * n);
	}

	static int add(int x, int y) {
		int res = x + y;
		return res;
	}

	public static void main(String[] args) {
		square(3);

		int sum = add(3, 6);
		System.out.println(sum);  // 9
		
		sum = add(4,7);
		System.out.println(sum); // 11
	}
}
