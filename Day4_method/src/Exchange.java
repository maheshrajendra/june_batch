
public class Exchange {
	void convertDollarToINR(double rate, int dollar) {
		System.out.println(rate * dollar);
	}

	public static void main(String[] args) {
		Exchange e = new Exchange();
		e.convertDollarToINR(76.5, 10); // 765.0

		e.convertDollarToINR(75.5, 280); // 21140.0
		
		// if you want to call a method directly 
		// without object and object reference
		// then make the method static   x3
	}
}
