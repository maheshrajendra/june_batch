
public class Person {
	static void greet(String str) {
		System.out.println("Hello " + str);
	}

	public static void main(String[] args) {
		greet("Mahesh MR");

		greet("Alpha");

		greet("Mark");
	}

}
