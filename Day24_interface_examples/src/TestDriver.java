
public class TestDriver {
	public static void main(String[] args) {
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com");
		driver.close();
		if (driver instanceof FirefoxDriver) {
			FirefoxDriver f = (FirefoxDriver) driver;
			f.openPrivateWindow();
		} else if (driver instanceof ChromeDriver) {
			ChromeDriver c = (ChromeDriver) driver;
			c.openIncognito();
		}
	}
}
