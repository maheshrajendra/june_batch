package jdbcexample;

public class MySql implements JDBC {

	@Override
	public void getConnection(String username, String password, int portNumber) {
		System.out.println("connected to mysql server");
	}

	@Override
	public void getTables() {
		System.out.println("retrieving the tables using cmd show tables;");
	}

}
