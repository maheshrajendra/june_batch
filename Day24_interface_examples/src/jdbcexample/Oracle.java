package jdbcexample;

public class Oracle implements JDBC {

	@Override
	public void getConnection(String username, String password, int portNumber) {
		System.out.println("Connecting to oracle database");
	}

	@Override
	public void getTables() {
		System.out.println("Retreving tables using select * from tab;");
	}

}
