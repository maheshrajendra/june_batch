
public interface WebDriver {
	
	// you get two keywords added by compiler
	// 1. public
	// 2. abstract
	void get(String url);
	
	void close();
}
