
public class ChromeDriver implements WebDriver {

	@Override
	public void get(String url) {
		System.out.println("Opening the url in the chrome browser");
	}

	@Override
	public void close() {
		System.out.println("closing the browser window");
	}
	
	public void openIncognito() {
		System.out.println("Opening the incognito window");
	}

}
