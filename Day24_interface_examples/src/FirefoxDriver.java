
public class FirefoxDriver implements WebDriver {

	@Override
	public void get(String url) {
		System.out.println("Opening the url in the firefox browser");
	}

	@Override
	public void close() {
		System.out.println("Closing the firefox browser window");
	}

	public void openPrivateWindow() {
		System.out.println("opens private window");
	}
}
