
public class House {
	String color;
	String noOfRooms;
	String typeOfHouse;
	double cost;

	// overriding the toString() - ALT + SHIFT + S + S
	@Override
	public String toString() {
		return "House [color=" + color + "]";
	}

	public static void main(String[] args) {
		House h1 = new House();
		h1.color = "Blue";
		House h2 = new House();
		System.out.println(h1);
	}

}
