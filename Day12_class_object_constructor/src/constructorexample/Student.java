package constructorexample;

public class Student {
	String name;
	int age;
	char gender;

	Student(String name, int age, char gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", gender=" + gender + "]";
	}

	public static void main(String[] args) {
		Student s = new Student("Alpha", 23, 'F');
		System.out.println(s);

		Student s2 = new Student("Beta", 25, 'M');
		System.out.println(s2);
	}

}
