package constructorexample;

public class Pen {
	String color;

	// Generate Constuctor using fields -> alt + shift + S + o
	// 1 parameterized custom constructor(user defined constructor)
	Pen(String color) {
		this.color = color;
	}
	

	// Override the toString -> alt + shift + s + s
	@Override
	public String toString() {
		return "Pen [color=" + color + "]";
	}

	public static void main(String[] args) {
		Pen p1 = new Pen("Green");
		System.out.println(p1);

		Pen p2 = new Pen("Red");
		System.out.println(p2);

		Pen p3 = new Pen("Blue");
		System.out.println(p3);

		// the below line will not work, as the program does not have zero parameterized
		// constructor
		//Pen p4 = new Pen();
		 
	}
}
