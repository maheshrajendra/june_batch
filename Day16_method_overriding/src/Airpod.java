
public class Airpod {
	String color;
	double price;
	String brand;

	public Airpod(String color, double price, String brand) {
		super();
		this.color = color;
		this.price = price;
		this.brand = brand;
	}

	@Override
	public String toString() {
		return color;
	}

	public static void main(String[] args) {
		Airpod a = new Airpod("Black", 2300, "PTron");
		System.out.println(a);
	}
}
