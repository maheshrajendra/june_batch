package methodoverridingexamples.animalexample;

public class TestAnimal {
	public static void main(String[] args) {
		Elephant e = new Elephant();
		e.eat();

		Dog d = new Dog();
		d.eat();

		Monkey m = new Monkey();
		m.eat();
	}
}
