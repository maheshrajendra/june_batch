package methodoverridingexamples.animalexample;

public class Elephant extends Animal {
	@Override
	void eat() {
		System.out.println("Eat with help of trunk");
	}
}
