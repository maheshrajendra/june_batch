package methodoverridingexamples.atmexamples;

public class TestATM {
	public static void main(String[] args) {
		SbiATM s = new SbiATM();
		s.withdraw();

		System.out.println("________________________________________________");
		DbsATM d = new DbsATM();
		d.withdraw();
	}
}
