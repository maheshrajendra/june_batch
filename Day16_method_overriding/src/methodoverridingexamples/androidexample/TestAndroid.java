package methodoverridingexamples.androidexample;

public class TestAndroid {
	public static void main(String[] args) {
		OnePlus o = new OnePlus();
		o.displayAnimation();

		System.out.println("_____________________________");

		Google g = new Google();
		g.displayAnimation();
	}
}
