package constructoroverloading;

public class Laptop {
	String brand = "Apple";
	String color;
	double screenSize;

	public Laptop(String color, double screenSize) {
		this.color = color;
		this.screenSize = screenSize;
	}

	public Laptop(String color) {
		this(color, 13.1);
	}

	@Override
	public String toString() {
		return "Laptop [brand=" + brand + ", color=" + color + ", screenSize=" + screenSize + "]";
	}

	public static void main(String[] args) {
		Laptop laptop = new Laptop("White", 14.5);
		System.out.println(laptop);

		Laptop laptop2 = new Laptop("Silver");
		System.out.println(laptop2);
	}

}
