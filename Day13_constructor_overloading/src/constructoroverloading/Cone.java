package constructoroverloading;

public class Cone {
	String flavour;
	double price;
	char size;

	Cone(String flavour) {
		this(flavour, 50, 'R');
	}

	Cone(String flavour, char size) {
		this.flavour = flavour;
		this.size = size;
		if (size == 'R') {
			this.price = 50;
		} else if (size == 'M') {
			this.price = 75;
		} else if (size == 'L') {
			this.price = 100;
		}
	}

	Cone(String flavour, double price, char size) {
		this.flavour = flavour;
		this.price = price;
		this.size = size;
	}

	@Override
	public String toString() {
		return "Cone [flavour=" + flavour + ", price=" + price + ", size=" + size + "]";
	}

	public static void main(String[] args) {
		Cone c1 = new Cone("Butterscoth", 'L');
		System.out.println(c1);

		Cone c = new Cone("Chocolate");
		System.out.println(c);
	}

}
