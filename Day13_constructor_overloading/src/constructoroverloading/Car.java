package constructoroverloading;

public class Car {
	String color;
	double price;
	String brand = "BMW";
	char variant;

	// ALT + SHIFT + S + O -> to generate constructor using fields
	Car(String color, char variant, double price) {
		this.color = color;
		this.variant = variant;
		this.price = price;
	}

	Car(String color, double price) {
		this(color, 'D', price);
	}

	Car(String color, char variant) {
		this(color, variant, 5000000);
	}

	// Overriding the toString() -> ALT + SHIFT + S + S
	@Override
	public String toString() {
		return "Car [color=" + color + ", price=" + price + ", brand=" + brand + ", variant=" + variant + "]";
	}

	public static void main(String[] args) {
		Car c1 = new Car("blue", 'E');
		System.out.println(c1);

		Car c2 = new Car("Red", 'P', 7000000);
		System.out.println(c2);

		// there is no zero param constructor in the program
		// Car c3 = new Car();

		Car c3 = new Car("Black", 4000000);
		System.out.println(c3);

	}

}
