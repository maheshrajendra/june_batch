
public class SampleBlock {

	static {
		System.setProperty("webdriver.chrome.driver", "c:/driver/chrome.exe");
	}
	
	static{
		System.out.println("Static block2");
	}

	{
		System.out.println("instance block 1");
	}

	public static void main(String[] args) {
		System.out.println("main method");
		SampleBlock s = new SampleBlock();
		SampleBlock s2 = new SampleBlock();
	}
}
