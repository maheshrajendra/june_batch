
public class SampleImpl implements Interface2 {

	@Override
	public void show() {
		System.out.println("Show method");
	}

	@Override
	public void print() {
		System.out.println("Print method");
	}

	public static void main(String[] args) {
		Interface2 s = new SampleImpl();
		s.print();
		s.show();
	}
}
