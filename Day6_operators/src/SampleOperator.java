
public class SampleOperator {
	public static void main(String[] args) {
		int x = 25;
		int y = ++x + x++ + x++;
		int z = y-- * x++;

		System.out.println(x);
		System.out.println(y);
		System.out.println(z);

		System.out.println(200 % 500);
		System.out.println(1 % 10);
		System.out.println(5 % 10);
		System.out.println(100 % 200);

	}
}
