
public class SampleForLoop {
	public static void main(String[] args) {
		for (int x = 1; x <= 5; x++) {
			System.out.println(x);
		}

		System.out.println("________________________________________________");
		for (int i = 10; i >= 1; i--) {
			System.out.println(i);
		}
	}
}
