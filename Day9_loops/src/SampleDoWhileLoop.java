import java.util.Scanner;

public class SampleDoWhileLoop {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		String confirm = null;
		do {
			System.out.println("Enter two numbers");
			int a = s.nextInt();
			int b = s.nextInt();
			int sum = a + b;
			System.out.println("The sum is " + sum);
			System.out.println("Do you want to perform another addition yes or no");
			confirm = s.next();
		} while (confirm.equalsIgnoreCase("yes"));
	}
}
