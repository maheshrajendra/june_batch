import java.util.ArrayList;
import java.util.Collections;

public class Laptop implements Comparable<Laptop> {
	String brand;
	int price;
	double rating;

	@Override
	public int compareTo(Laptop o) {
		return Integer.compare(this.price, o.price);

		// other logics you can write
		// return this.price-o.price;

		// Integer i1 = this.price;
		// Integer i2 = o.price;
		// return i1.compareTo(i2);
	}

	// override the toString

	// create 3 param constructor

	public static void main(String[] args) {
		// create 5 laptop object
		// insert into linkedList
		// sort it using Collections.sort method and display the output
		ArrayList<Laptop> list = new ArrayList<>();
		
		RatingComparator rc = new RatingComparator();
		
		Collections.sort(list, rc);
	}

}
