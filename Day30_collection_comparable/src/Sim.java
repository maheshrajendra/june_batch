import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class Sim implements Comparable<Sim> {
	long serialNo;
	int phoneNo;

	public Sim(long serialNo, int phoneNo) {
		this.serialNo = serialNo;
		this.phoneNo = phoneNo;
	}

	@Override
	public String toString() {
		return "Sim [serialNo=" + serialNo + ", phoneNo=" + phoneNo + "]\n";
	}

	public static void main(String[] args) {
		Sim s1 = new Sim(23582368263L, 986321123);
		Sim s2 = new Sim(23582368267L, 986321121);
		Sim s3 = new Sim(23582368261L, 986321127);
		Sim s4 = new Sim(23582368264L, 986321125);

		LinkedList<Sim> simList = new LinkedList<>(Arrays.asList(s1, s2, s3, s4));
		System.out.println(simList);

		Collections.sort(simList);

		System.out.println(simList);
	}

	@Override
	public int compareTo(Sim o) {
		return -Long.compare(this.serialNo, o.serialNo);
		
		// Long first = this.serialNo;
		// Long second = o.serialNo;
		// return first.compareTo(second);
	}

}
