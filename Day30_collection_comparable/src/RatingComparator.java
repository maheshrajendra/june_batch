import java.util.Comparator;

public class RatingComparator implements Comparator<Laptop> {

	@Override
	public int compare(Laptop o1, Laptop o2) {
		return Double.compare(o1.rating, o2.rating);
	}

}
