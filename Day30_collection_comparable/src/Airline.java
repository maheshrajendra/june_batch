import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Airline implements Comparable<Airline> {
	String name;
	double price;

	public Airline(String name, double price) {
		this.name = name;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Airline [name=" + name + ", price=" + price + "]\n";
	}

	public static void main(String[] args) {
		Airline a1 = new Airline("Jet Airways", 8500.0);
		Airline a2 = new Airline("Air Asia", 3500.0);
		Airline a3 = new Airline("Etihad Airways", 9500.0);
		Airline a4 = new Airline("Indigo", 5500.0);

		ArrayList<Airline> flights = new ArrayList<>(Arrays.asList(a1, a2, a3, a4));

		System.out.println(flights);

		PriceComparator pc = new PriceComparator();
		Collections.sort(flights, pc );

		System.out.println("After sorting");
		System.out.println(flights);
	}

	
	// default sorting
	@Override
	public int compareTo(Airline a) {
		return -(this.name.compareTo(a.name));
	}
}
