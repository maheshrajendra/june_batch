import java.util.Comparator;

public class PriceComparator implements Comparator<Airline> {

	// custom sorting
	@Override
	public int compare(Airline o1, Airline o2) {
		return Double.compare(o1.price, o2.price);
	}

}
