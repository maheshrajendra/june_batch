import java.util.Arrays;

public class ModifyArraySecondLogic {
	public static void main(String[] args) {
		int[] nums = new int[50];
		for (int i = 0; i < nums.length; i++) {
			nums[i] = (i + 1) * 10;
		}

		// display the array // ctrl + shift + O(Owe) is for importing

		System.out.println(Arrays.toString(nums));

		/*
		 * java.util.Arrays is a predefined class, which provides methods to work on
		 * array objects toString is a static method in the Arrays class, which builds
		 * the String object with the contents stored in the array
		 */
	}
}
