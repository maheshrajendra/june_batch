
public class ModifyArray {
	public static void main(String[] args) {
		int[] nums = new int[50];
		int temp = 0;

		printArrayElement(nums);
		// modifying the array object
		for (int i = 0; i < nums.length; i++) {
			nums[i] = temp + 10;
			temp = nums[i];
		}

		// displaying the array
		printArrayElement(nums);
		printArrayElement(nums);
		printArrayElement(nums);
		printArrayElement(nums);

	}

	static void printArrayElement(int[] x) {
		for (int i : x) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
}
