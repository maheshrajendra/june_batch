package usingiterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

public class SamplePrintingUsingIterator {
	public static void main(String[] args) {
		ArrayList<Integer> a = new ArrayList<>(Arrays.asList(10, 20, 30, 40, 50));
		log.trace();
		Iterator<Integer> itr = a.iterator();
		while (itr.hasNext()) {
			Integer x = (Integer) itr.next();
			System.out.println(x);
		}

		// for each loop
		for (Integer x : a) {
			System.out.println(x);
		}

		TreeSet<Integer> t = new TreeSet<>(a);
		for (Integer i : t) {
			System.out.println(i);
		}

		t.forEach(i -> System.out.println(i));
	}
}
