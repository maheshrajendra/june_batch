package vehicleexample;

public class Bike extends Vehicle {
	boolean isKickerPresent;

	void kick() {
		System.out.println("Bike kick start");
	}

	@Override
	public String toString() {
		return "Bike [color=" + color + ", brand=" + brand + ", price=" + price + ", variant=" + variant
				+ ", isKickerPresent=" + isKickerPresent + "]";
	}

}
