package vehicleexample;

public class Car extends Vehicle {

	
	boolean isAutomatic;

	void autoPilot() {
		System.out.println("Car driving on auto pilot");
	}

	@Override
	public String toString() {
		return "Car [color=" + color + ", brand=" + brand + ", price=" + price + ", variant=" + variant
				+ ", isAutomatic=" + isAutomatic + "]";
	}

	

}
