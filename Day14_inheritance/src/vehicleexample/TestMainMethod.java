package vehicleexample;

public class TestMainMethod {
	public static void main(String[] args) {
		Car c = new Car();
		c.brand = "Rolls Royce";
		c.color = "White";
		c.isAutomatic = true;
		c.price = 7500000;
		c.variant = "Petrol";
		System.out.println(c);
		c.start();
		c.autoPilot();
		c.stop();

		System.out.println("_______________________________________");

		Truck t = new Truck();
		t.brand = "Eicher";
		t.color = "Brown";
		t.price = 5000000;
		t.variant = "Diesel";
		t.loadCapacity = 3000;
		System.out.println(t);
		t.start();
		t.dumpLoad();
		t.stop();

		System.out.println("_______________________________________");

		Bike b = new Bike();
		b.brand = "Kawasaki";
		b.color = "Green";
		b.price = 1000000;
		b.variant = "Petrol";
		b.isKickerPresent = false;
		System.out.println(b);
		b.start();
		b.kick();
		b.stop();
	}
}
