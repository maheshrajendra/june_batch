package vehicleexample;

public class Truck extends Vehicle {

	int loadCapacity;

	void dumpLoad() {
		System.out.println("Truck dumps load");
	}

	@Override
	public String toString() {
		return "Truck [color=" + color + ", brand=" + brand + ", price=" + price + ", variant=" + variant
				+ ", loadCapacity=" + loadCapacity + "]";
	}

}
