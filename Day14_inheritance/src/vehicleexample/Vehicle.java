package vehicleexample;

public class Vehicle {
	String color;
	String brand;
	double price;
	String variant;

	void start() {
		System.out.println("Vehicle starts");
	}

	void stop() {
		System.out.println("Vehicle stops");
	}
	
}
