import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Employee implements Comparable<Employee> {
	String name;
	int id;
	long phoneNumber;

	// constructor
	public Employee(String name, int id, long phoneNumber) {
		this.name = name;
		this.id = id;
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", id=" + id + ", phoneNumber=" + phoneNumber + "]\n";
	}

	public static void main(String[] args) {
		Employee e1 = new Employee("Mark", 24, 93736353526L);
		Employee e2 = new Employee("Alpha", 21, 93736353521L);
		Employee e3 = new Employee("Charlie", 20, 93736353528L);

		ArrayList<Employee> employeeList = new ArrayList<>(Arrays.asList(e1, e2, e3));
		System.out.println(employeeList);

		Collections.sort(employeeList);
		System.out.println("after sorting on ID");
		System.out.println(employeeList);

		System.out.println("Sorting the employee object using name ascending order");
		Comparator<Employee> nc = (x, y) -> {
			return x.name.compareTo(y.name);
		};
		
		// Comparator<Employee> nameComparator = (x, y) -> x.name.compareTo(y.name);

		Collections.sort(employeeList, nc);
		System.out.println(employeeList);

		Comparator<Employee> pc = (x, y) -> {
			return Long.compare(x.phoneNumber, y.phoneNumber);
		};
		
		Collections.sort(employeeList, pc);
		System.out.println("Sorting the employee object using phone number ascending order");
		System.out.println(employeeList);

	}

	@Override
	public int compareTo(Employee o) {
		return Integer.compare(this.id, o.id);
	}

}
