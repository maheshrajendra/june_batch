import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Set;

public class SampleHashMap {
	public static void main(String[] args) {
		LinkedHashMap<String, Long> map = new LinkedHashMap<>();
		map.put("Alpha", 98642234434233L);
		map.put("Beta", 932642369263897L);
		map.put("Charlie", 43653659365645L);

		// using a forEach for printing only keys/ values or both
		map.forEach((k, v) -> System.out.println(v));

		// keyset method for printing only keys
		Set<String> keys = map.keySet();
		for (String key : keys) {
			System.out.println(key);
		}

		// values method for printing only values
		System.out.println("________________________________________________");
		Collection<Long> values = map.values();
		for (Long value : values) {
			System.out.println(value);
		}

	}
}
