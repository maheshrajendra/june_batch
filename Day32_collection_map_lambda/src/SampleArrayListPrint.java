import java.util.ArrayList;
import java.util.Arrays;

public class SampleArrayListPrint {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>(Arrays.asList("Red", "Green", "Blue"));
		list.forEach(color -> System.out.println(color));
	}

}
