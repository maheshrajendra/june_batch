
public class Calculator {
	int add(int x, int y) {
		return x + y;
	}

	int add(int x, int y, int z) {
		return x + y + z;
	}

	double add(double a, double b) {
		return a + b;
	}

	void add(int a, double b) {

	}

	void add(double a, int b) {

	}

	public static void main(String[] args) {
		// to invoke(call) the methods of the class
		// we must create object and access using dot operator

		Calculator c = new Calculator();
		int result = c.add(45, 65);
		System.out.println(result);

		double d = c.add(10.5, 2.5);
	}
}
