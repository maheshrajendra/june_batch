
public class Facebook {
	void login(String emailAddress, String password) {
		System.out.println("Logged in using email address");
	}

	void login(long phoneNumber, String password) {
		System.out.println("Logged in using phone number");
	}

	public static void main(String[] args) {
		Facebook f = new Facebook();
		f.login("alpha@gmail.com", "Alpha@123");
		f.login(234234234234L, "Alpha@123");
	}
}
