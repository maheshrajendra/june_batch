
public class ForEachExample {
	public static void main(String[] args) {
		String[] names = { "Alpha", "Beta", "Charlie" };
		for (String name : names) {
			System.out.println("Hello " + name);
		}

		System.out.println("____________________________________");

		int[] nums = { 10, 20, 30, 40, 50 };
		for (int num : nums) {
			System.out.print(num + "\t");
		}

		System.out.println("\n____________________________________");

		char[] chs = { 'H', 'e', 'l', 'l', 'o' };
		for (char c : chs) {
			System.out.print(c + " ");
		}

	}
}
