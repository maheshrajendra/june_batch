package com.mr.associationexample.studentexample;
public class Address {
	int houseNo;
	String street;
	String city;
	String state;
	int pinCode;

	@Override
	public String toString() {
		return "Address [houseNo=" + houseNo + ", street=" + street + ", city=" + city + ", state=" + state
				+ ", pinCode=" + pinCode + "]";
	}

}
