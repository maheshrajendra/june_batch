package com.mr.associationexample.studentexample;

public class Student {
	int id;
	int age;
	long phoneNo;
	Address address;

	@Override
	public String toString() {
		return "Student [id=" + id + ", age=" + age + ", phoneNo=" + phoneNo + ", address=" + address + "]";
	}

	public static void main(String[] args) {
		Student s = new Student();
		s.id = 45;
		s.age = 21;
		s.phoneNo = 98764217236L;
		s.address = new Address();
		s.address.houseNo = 4;
		s.address.street = "1st main";
		s.address.city = "Bangalore";
		s.address.state = "Karnataka";
		s.address.pinCode = 560006;
		System.out.println(s);

	}
}
