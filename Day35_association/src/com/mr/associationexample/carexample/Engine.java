package com.mr.associationexample.carexample;

public class Engine {
	String chasisNo;
	String engineNo;

	@Override
	public String toString() {
		return "Engine [chasisNo=" + chasisNo + ", engineNo=" + engineNo + "]";
	}

}
