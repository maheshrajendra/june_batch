package com.mr.associationexample.carexample;

public class Car {
	String regNo;
	int seatingCapacity;
	String color;
	Engine engine;

	@Override
	public String toString() {
		return "Car [regNo=" + regNo + ", seatingCapacity=" + seatingCapacity + ", color=" + color + ", engine="
				+ engine + "]";
	}

	public static void main(String[] args) {
		Car c1 = new Car();
		c1.regNo = "KA93S2234";
		c1.seatingCapacity = 4;
		c1.color = "Red";
		c1.engine = new Engine();
		c1.engine.chasisNo = "346585hsldjflsjf";
		c1.engine.engineNo = "dshfushf234h2423";
		System.out.println(c1);
	}

}
