import java.util.HashMap;

public class SampleHashMapExample {
	public static void main(String[] args) {
		HashMap<Integer, String> map = new HashMap<>();
		map.put(41, "Alpha");
		map.put(2, "Charlie");
		map.put(3, "Delta");

		System.out.println(map); // {2=Charlie, 3=Delta, 41=Alpha}
		System.out.println(map.get(2));
		map.put(2, "Mike");
		System.out.println(map); // {2=Mike, 3=Delta, 41=Alpha}
		System.out.println(map.containsKey(41)); // true
		System.out.println(map.containsValue("Charlie")); // false

		map.remove(3);

		System.out.println(map.size()); // 2
		System.out.println(map); // {2=Mike, 41=Alpha}

		map.clear();

		System.out.println(map); // {}

	}
}
