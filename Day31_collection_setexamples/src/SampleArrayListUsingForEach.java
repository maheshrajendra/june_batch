import java.util.ArrayList;
import java.util.Arrays;

public class SampleArrayListUsingForEach {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>(Arrays.asList(56, 23, 48, 16, 93));

		for (Integer i : list) {
			System.out.println(i);
		}
		
		
		System.out.println(list); // [56, 23, 48, 16, 93]
	}
}
