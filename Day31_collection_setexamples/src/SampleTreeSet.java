import java.util.Collections;
import java.util.TreeSet;

public class SampleTreeSet {
	public static void main(String[] args) {
		TreeSet<Integer> nums = new TreeSet<>(Collections.reverseOrder());
		nums.add(1);
		nums.add(45);
		nums.add(20);
		nums.add(25);
		System.out.println(nums); 
	}
}
