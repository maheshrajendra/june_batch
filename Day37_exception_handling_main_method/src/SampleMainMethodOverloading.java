
public class SampleMainMethodOverloading {
	public static void main(String[] args) {
		System.out.println("Hello");
		main(45);
		main("");
	}

	public static void main(String args) {
		System.out.println("String method");
	}

	public static void main(int args) {
		System.out.println("int method");
	}
}
