
public class Product {

	@Override
	protected void finalize() throws Throwable {
		System.out.println("Called the finalize method");
	}

	public static void main(String[] args) {
		Product p = new Product();
		Product p2 = new Product();
		Product p3 = new Product();
		p = null;
		p2 = null;
		p3 = null;
		System.gc();
		try {
			Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
