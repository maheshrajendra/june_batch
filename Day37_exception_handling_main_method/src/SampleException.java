
public class SampleException {
	public static void main(String[] args) {
		try {
			int a = 10;
			int b = 5;
			int c = a / b; // throw new ArithmeticException();
			System.out.println(c);
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
		System.out.println("Completely executed");
	}
}
