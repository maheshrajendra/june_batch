import java.util.InputMismatchException;
import java.util.Scanner;

public class Example {
	public static void main(String[] args) {
		Scanner s = null;
		System.out.println("Enter two num");
		try {
			s = new Scanner(System.in);
			int a = s.nextInt();
			int b = s.nextInt();
			int c = a / b;
			System.out.println(c);
		} catch (InputMismatchException e) {
			System.out.println("Please enter a correct value");
		} catch (ArithmeticException e) {
			System.out.println("Please provide a correct denominator");
		} catch (Exception e) {
			System.out.println("Some other problem occured");
		} finally {
			s.close();
		}
		System.out.println("Successful completion");
	}
}
