
public class DummyStackOverflow {
	public static void main(String[] args) {
		System.out.println("main method");
		x();
	}

	static void x() {
		System.out.println("X");
		y();
	}

	static void y() {
		System.out.println("y");
		x();
	}
}
