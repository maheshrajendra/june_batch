
public class UncheckedExceptionExample {
	public static void main(String[] args) {
		print(null);
	}

	static void print(String name) {
		if (name != null) {
			System.out.println(name.toUpperCase());
		}
	}
}
