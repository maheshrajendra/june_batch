Object:
Object class is the super most class in java, it becomes directly or indirectly the
super class of every class that we create in java

-> it is present in java.lang package
-> it has no variables
-> it has one zero parameterized constructor
-> it has around 11 methods

-> important methods are
	1. public String toString()
	2. public boolean equals(Object obj)
	3. public int hashCode()
