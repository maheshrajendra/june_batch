package com.mr.superchainingexamples.objectclass;

public class Student {
	int sno;

	void study() {
		System.out.println("Studying...");
	}
	
	
	public static void main(String[] args) {
		Student s = new Student();
		// put s. and see what variables and method would be accessible here
	}
}
