package com.mr.superchainingexamples;

public class Vehicle {
	String color;
	int hp;

	void start() {
		System.out.println("Vehicle starts");
	}

	void stop() {
		System.out.println("Vehicle stops");
	}

	Vehicle(String color, int hp) {
		this.color = color;
		this.hp = hp;
	}

}
