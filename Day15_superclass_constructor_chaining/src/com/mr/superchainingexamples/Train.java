package com.mr.superchainingexamples;

public class Train extends Vehicle {

	Train(String color, int hp) {
		super(color, hp);
	}

	@Override
	public String toString() {
		return "Train [color=" + color + ", hp=" + hp + "]";
	}

	public static void main(String[] args) {
		Train t = new Train("Green", 5600);
		System.out.println(t);

		Train t2 = new Train("Red", 9800);
		System.out.println(t2);
	}

}
