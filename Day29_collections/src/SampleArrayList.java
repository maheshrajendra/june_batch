import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SampleArrayList {

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>(Arrays.asList("Red", "Blue", "Green", "Yellow", "Black"));

		System.out.println(list); // [Red, Blue, Green, Yellow, Black]

		// Collections.sort(list);
		Collections.sort(list, Collections.reverseOrder());

		System.out.println("After sorting");
		System.out.println(list); // [Black, Blue, Green, Red, Yellow]
	}
}
