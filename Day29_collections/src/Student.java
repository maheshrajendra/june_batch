import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Student implements Comparable<Student> {
	int id;
	String name;

	@Override
	public int compareTo(Student s) {
		return this.id - s.id;
	}

	// alt + shift + s + o
	public Student(int id, String name) {
		this.id = id;
		this.name = name;
	}

	// alt + shift + S+ S
	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + "]\n";
	}

	public static void main(String[] args) {
		Student s1 = new Student(4, "Charlie");
		Student s2 = new Student(1, "Delta");
		Student s3 = new Student(2, "Alpha");
		Student s4 = new Student(3, "Mike");

		ArrayList<Student> studentList = new ArrayList<>(Arrays.asList(s1, s2, s3, s4));
		System.out.println(studentList);

		Collections.sort(studentList);

		// after sorting
		System.out.println(studentList);
	}
}
