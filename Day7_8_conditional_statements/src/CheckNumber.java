
public class CheckNumber {
	public static void main(String[] args) {
		int n = 99;
		if (n < 0) {
			System.out.println("Negative");
		} else if (n > 0) {
			System.out.println("Positive");
		} else {
			System.out.println("Equal");
		}

		System.out.println((n < 0) ? "Negative" : (n > 0) ? "Positive" : "Equal");
	}
}
