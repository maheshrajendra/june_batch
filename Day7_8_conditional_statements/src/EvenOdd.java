
public class EvenOdd {
	public static void main(String[] args) {
		int number = 11;
		if (number % 2 == 0) {
			System.out.println(number + " is a Even number");
		} else {
			System.out.println(number + " is a Odd number");
		}

		System.out.println(number + ((number % 2 == 0) ? " is even number" : " is odd number"));
	}
}
