
public class MarksCalculator {
	public static void main(String[] args) {
		int avgMarks = 50;
		if (avgMarks >= 75) {
			System.out.println("Distinction");
		} else if (avgMarks >= 60 && avgMarks < 75) {
			System.out.println("First class");
		} else if (avgMarks >= 50 && avgMarks < 60) {
			System.out.println("Second class");
		} else if (avgMarks >= 35 && avgMarks < 50) {
			System.out.println("Pass");
		} else {
			System.out.println("fail");
		}
	}
}
