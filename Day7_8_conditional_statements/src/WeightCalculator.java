
public class WeightCalculator {
	public static void main(String[] args) {
		int age = 65;
		int weight = 85;
		if (age > 30) {
			if (weight > 70) {
				System.out.println("overweight");
			} else {
				System.out.println("underweight");
			}
		}

		System.out.println((age > 30) ? (weight > 70) ? "Overweight" : "Underweight" : "");
	}
}
